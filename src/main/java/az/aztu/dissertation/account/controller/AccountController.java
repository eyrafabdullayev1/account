package az.aztu.dissertation.account.controller;

import az.aztu.dissertation.account.dto.request.AccountAddBrandNewRequestDto;
import az.aztu.dissertation.account.dto.response.CommonResponse;
import az.aztu.dissertation.account.mappers.AccountMapper;
import az.aztu.dissertation.account.service.AccountService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

import static az.aztu.dissertation.account.enums.ErrorMessage.SUCCESS;

@RestController
@RequestMapping("/api/v1/accounts")
public class AccountController {

    private final AccountService accountService;

    public AccountController(AccountService accountService) {
        this.accountService = accountService;
    }

    @GetMapping("/account/get/by/accountValue/{accountValue}/and/accountType/{accountType}")
    public ResponseEntity<CommonResponse<?>> getByAccountValueAndAccountType(
            @PathVariable String accountValue,
            @PathVariable String accountType
    ) {
        return new CommonResponse<>().success(
                SUCCESS.getMessageBody(),
                AccountMapper.INSTANCE.toResponseDto(
                        accountService.getByAccountValueAndAccountType(accountValue, accountType)
                )
        );
    }

    @PutMapping("/account/add")
    public ResponseEntity<CommonResponse<?>> addAccount(
            @RequestBody AccountAddBrandNewRequestDto request
    ) {
        // TODO: validate duplicated account value and type
        accountService.insertOrUpdate(
                AccountMapper.INSTANCE.fromRequestToDto(
                        request
                )
        );
        return new CommonResponse<>().success(SUCCESS.getMessageBody(), null);
    }

    @PatchMapping("/account/update")
    public ResponseEntity<CommonResponse<?>> updateAndSaveAccount(
            @RequestParam Long id,
            @RequestBody Map<String, Object> fieldsMap
    ) {
        accountService.updateAndSave(id, fieldsMap);
        return new CommonResponse<>().success(SUCCESS.getMessageBody(), null);
    }
}
