package az.aztu.dissertation.account.repository;

import az.aztu.dissertation.account.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends JpaRepository<AccountEntity, Long> {

    @Query(value = "SELECT * FROM account WHERE account_value = ?1 AND account_type = ?2", nativeQuery = true)
    Optional<AccountEntity> findByAccountValueAndAccountType(String accountValue, String accountType);
}
