package az.aztu.dissertation.account.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class ObjectUtil {

    private final ObjectMapper objectMapper;

    public ObjectUtil(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public <T> T parse(Object fromValue, Class<T> toValueType) {
        try {
            return objectMapper.convertValue(fromValue, toValueType);
        } catch (Exception ex) {
            return null;
        }
    }
}
