package az.aztu.dissertation.account.util;

import az.aztu.dissertation.account.config.ApplicationProperties;
import az.aztu.dissertation.account.dto.AuthUserDetailsDto;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class TokenUtil {

    private final ApplicationProperties applicationProperties;

    public TokenUtil(ApplicationProperties applicationProperties) {
        this.applicationProperties = applicationProperties;
    }

    private final Clock clock = DefaultClock.INSTANCE;

    public String getUsernameFromToken(String token) {
        return getClaimsFromToken(token, Claims::getSubject);
    }

    public Date getIssuedAtFromToken(String token) {
        return getClaimsFromToken(token, Claims::getIssuedAt);
    }

    public Date getExpirationDateFromToken(String token) {
        return getClaimsFromToken(token, Claims::getExpiration);
    }

    private boolean isTokenExpired(String token) {
        final Date expirationDate = getExpirationDateFromToken(token);
        return expirationDate.before(clock.now());
    }

    public boolean validateToken(String token, UserDetails userDetails) {
        AuthUserDetailsDto authUserDetails = (AuthUserDetailsDto) userDetails;
        final String username = getUsernameFromToken(token);
        return (username.equals(authUserDetails.getUsername()) && !isTokenExpired(token));
    }

    public <T> T getClaimsFromToken(String token, Function<Claims, T> claimsResolver) {
        Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }

    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser()
                   .setSigningKey(applicationProperties.getTokenJWTSigningKey())
                   .parseClaimsJws(token)
                   .getBody();
    }

    public String generateToken(UserDetails userDetails) {
        HashMap<String, Object> claimsMap = new HashMap<>();
        return doGenerate(userDetails.getUsername(), claimsMap);
    }

    private String doGenerate(String subject, Map<String, Object> claimsMap) {
        final Date issuedAt = clock.now();
        final Date expirationDate = calculateExpirationDate(issuedAt);

        return Jwts.builder()
                   .setClaims(claimsMap)
                   .setSubject(subject)
                   .setIssuedAt(issuedAt)
                   .setExpiration(expirationDate)
                   .signWith(SignatureAlgorithm.HS512, applicationProperties.getTokenJWTSigningKey())
                   .compact();
    }

    private Date calculateExpirationDate(Date issuedAt) {
        return new Date(issuedAt.getTime() + applicationProperties.getTokenJWTExpirationInSeconds() * 1000);
    }
}
