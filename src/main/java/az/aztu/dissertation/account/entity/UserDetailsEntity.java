package az.aztu.dissertation.account.entity;

import az.aztu.dissertation.account.enums.UserAuthority;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "user_details")
public class UserDetailsEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "user_authority", nullable = false)
    @Enumerated(EnumType.STRING)
    private UserAuthority userAuthority;
    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity relatedUser;

    public Long getId() {
        return id;
    }

    public UserDetailsEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public UserAuthority getUserAuthority() {
        return userAuthority;
    }

    public UserDetailsEntity setUserAuthority(UserAuthority userAuthority) {
        this.userAuthority = userAuthority;
        return this;
    }

    public UserEntity getRelatedUser() {
        return relatedUser;
    }

    public UserDetailsEntity setRelatedUser(UserEntity user) {
        this.relatedUser = user;
        return this;
    }
}
