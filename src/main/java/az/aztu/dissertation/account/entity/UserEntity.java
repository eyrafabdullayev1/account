package az.aztu.dissertation.account.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class UserEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String surname;
    @Column(nullable = false)
    private String username;
    @Column(nullable = false)
    private String password;
    @OneToMany(
            mappedBy = "relatedUser",
            cascade = CascadeType.ALL,
            fetch = FetchType.EAGER
    )
    private List<UserDetailsEntity> userAuthorityList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public UserEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserEntity setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserEntity setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserEntity setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserEntity setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<UserDetailsEntity> getUserAuthorityList() {
        return userAuthorityList;
    }

    public UserEntity setUserAuthorityList(List<UserDetailsEntity> userAuthorityList) {
        this.userAuthorityList = userAuthorityList;
        return this;
    }
}
