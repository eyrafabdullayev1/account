package az.aztu.dissertation.account.entity;

import az.aztu.dissertation.account.enums.AccountType;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

@Entity
@Table(name = "account")
public class AccountEntity implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "account_value", nullable = false)
    private String accountValue;
    @Column(name = "account_type", nullable = false)
    @Enumerated(value = EnumType.STRING)
    private AccountType accountType;
    private BigDecimal balance;
    @Column(name = "is_expired", nullable = false)
    private Boolean isExpired;
    @OneToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private UserEntity user;

    public Long getId() {
        return id;
    }

    public AccountEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAccountValue() {
        return accountValue;
    }

    public AccountEntity setAccountValue(String accountValue) {
        this.accountValue = accountValue;
        return this;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountEntity setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public AccountEntity setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public AccountEntity setExpired(Boolean expired) {
        isExpired = expired;
        return this;
    }

    public UserEntity getUser() {
        return user;
    }

    public AccountEntity setUser(UserEntity user) {
        this.user = user;
        return this;
    }
}
