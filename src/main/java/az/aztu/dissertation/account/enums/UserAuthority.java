package az.aztu.dissertation.account.enums;

public enum UserAuthority {

    USER, ADMIN
}
