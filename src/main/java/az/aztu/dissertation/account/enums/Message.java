package az.aztu.dissertation.account.enums;

public enum Message {

    ERROR_TOKEN_UNABLE_USERNAME("Token unable to get username from it"),
    WARN_TOKEN_EXPIRED("Token expired"),
    WARN_TOKEN_DOES_NOT_START_WITH("Token does not start with '%s' string"),
    ERROR_USER_DISABLED("User disabled"),
    ERROR_DUPLICATED_PIN("Duplicated pin"),
    ERROR_INVALID_CREDENTIALS("Invalid credentials"),
    SUCCESS_USER_REGISTERED("User registered");

    private final String message;

    Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
