package az.aztu.dissertation.account.enums;

public enum AccountType {

    ACCOUNT, OTHER_ACCOUNT
}
