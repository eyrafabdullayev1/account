package az.aztu.dissertation.account.mappers;

import az.aztu.dissertation.account.dto.entity.UserDetailsDto;
import az.aztu.dissertation.account.dto.entity.UserDto;
import az.aztu.dissertation.account.entity.UserDetailsEntity;
import az.aztu.dissertation.account.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserDto toDto(UserEntity entity);

    default UserDetailsDto userDetailsEntityToUserDetailsDto(UserDetailsEntity userDetailsEntity) {
        if ( userDetailsEntity == null ) {
            return null;
        }

        UserDetailsDto userDetailsDto = new UserDetailsDto();

        userDetailsDto.setId( userDetailsEntity.getId() );
        userDetailsDto.setUserAuthority( userDetailsEntity.getUserAuthority() );
        userDetailsDto.setRelatedUser(
                userDetailsEntity.getRelatedUser() != null
                && userDetailsEntity.getRelatedUser().getId() != null
                        ? new UserDto().setId(userDetailsEntity.getRelatedUser().getId())
                        : new UserDto()
        );

        return userDetailsDto;
    }

    UserEntity toEntity(UserDto dto);

    default UserDetailsEntity userDetailsDtoToUserDetailsEntity(UserDetailsDto userDetailsDto) {
        if ( userDetailsDto == null ) {
            return null;
        }

        UserDetailsEntity userDetailsEntity = new UserDetailsEntity();

        userDetailsEntity.setId( userDetailsDto.getId() );
        userDetailsEntity.setUserAuthority( userDetailsDto.getUserAuthority() );
        userDetailsEntity.setRelatedUser(
                userDetailsDto.getRelatedUser() != null
                && userDetailsDto.getRelatedUser().getId() != null
                        ? new UserEntity().setId(userDetailsEntity.getRelatedUser().getId())
                        : new UserEntity()
        );

        return userDetailsEntity;
    }
}
