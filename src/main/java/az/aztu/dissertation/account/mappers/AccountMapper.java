package az.aztu.dissertation.account.mappers;

import az.aztu.dissertation.account.dto.entity.AccountDto;
import az.aztu.dissertation.account.dto.entity.UserDetailsDto;
import az.aztu.dissertation.account.dto.entity.UserDto;
import az.aztu.dissertation.account.dto.request.AccountAddBrandNewRequestDto;
import az.aztu.dissertation.account.dto.response.AccountResponseDto;
import az.aztu.dissertation.account.entity.AccountEntity;
import az.aztu.dissertation.account.entity.UserDetailsEntity;
import az.aztu.dissertation.account.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    AccountDto toDto(AccountEntity entity);

    default UserDetailsDto userDetailsEntityToUserDetailsDto(UserDetailsEntity userDetailsEntity) {
        if ( userDetailsEntity == null ) {
            return null;
        }

        UserDetailsDto userDetailsDto = new UserDetailsDto();

        userDetailsDto.setId( userDetailsEntity.getId() );
        userDetailsDto.setUserAuthority( userDetailsEntity.getUserAuthority() );
        userDetailsDto.setRelatedUser(
                userDetailsEntity.getRelatedUser() != null
                && userDetailsEntity.getRelatedUser().getId() != null
                        ? new UserDto().setId(userDetailsEntity.getRelatedUser().getId())
                        : new UserDto()
        );

        return userDetailsDto;
    }

    AccountEntity toEntity(AccountDto dto);

    default UserDetailsEntity userDetailsDtoToUserDetailsEntity(UserDetailsDto userDetailsDto) {
        if ( userDetailsDto == null ) {
            return null;
        }

        UserDetailsEntity userDetailsEntity = new UserDetailsEntity();

        userDetailsEntity.setId( userDetailsDto.getId() );
        userDetailsEntity.setUserAuthority( userDetailsDto.getUserAuthority() );
        userDetailsEntity.setRelatedUser(
                userDetailsDto.getRelatedUser() != null
                        && userDetailsDto.getRelatedUser().getId() != null
                        ? new UserEntity().setId(userDetailsDto.getRelatedUser().getId())
                        : new UserEntity()
        );

        return userDetailsEntity;
    }

    AccountResponseDto toResponseDto(AccountDto dto);
    default AccountDto fromRequestToDto(AccountAddBrandNewRequestDto requestDto) {
        if ( requestDto == null ) {
            return null;
        }

        AccountDto accountDto = new AccountDto();
        accountDto.setId(requestDto.getId());
        accountDto.setAccountValue(requestDto.getAccountValue());
        accountDto.setAccountType(requestDto.getAccountType());
        accountDto.setBalance(requestDto.getBalance());
        accountDto.setExpired(requestDto.getExpired());
        accountDto.setUser(new UserDto().setId(requestDto.getUserId()));

        return accountDto;
    }
}
