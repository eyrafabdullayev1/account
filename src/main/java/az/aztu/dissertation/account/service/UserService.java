package az.aztu.dissertation.account.service;

import az.aztu.dissertation.account.dto.entity.UserDto;
import az.aztu.dissertation.account.entity.UserDetailsEntity;
import az.aztu.dissertation.account.entity.UserEntity;
import az.aztu.dissertation.account.mappers.UserMapper;
import az.aztu.dissertation.account.repository.UserRepository;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private final UserRepository userRepository;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public UserDto getById(Long id) {
        return userRepository.findById(id)
                             .map(UserMapper.INSTANCE::toDto)
                             .orElseGet(UserDto::new);
    }

    public UserDto getByUsername(String username) {
        return userRepository.findByUsername(username)
                             .map(UserMapper.INSTANCE::toDto)
                             .orElseGet(UserDto::new);
    }

    public UserDto insertOrUpdate(UserDto userDto) {
        UserEntity userEntity = UserMapper.INSTANCE.toEntity(userDto);
        for(UserDetailsEntity userDetailsEntity : userEntity.getUserAuthorityList()) {
            userDetailsEntity.setRelatedUser(userEntity);
        }
        return UserMapper.INSTANCE.toDto(
                userRepository.save(userEntity)
        );
    }
}
