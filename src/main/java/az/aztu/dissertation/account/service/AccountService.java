package az.aztu.dissertation.account.service;

import az.aztu.dissertation.account.dto.entity.AccountDto;
import az.aztu.dissertation.account.dto.entity.UserDto;
import az.aztu.dissertation.account.exception.custom.BaseException;
import az.aztu.dissertation.account.exception.custom.ClientException;
import az.aztu.dissertation.account.mappers.AccountMapper;
import az.aztu.dissertation.account.repository.AccountRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Map;

import static az.aztu.dissertation.account.enums.ErrorMessage.ERROR_ACCOUNT_NOT_FOUND;
import static az.aztu.dissertation.account.enums.ErrorMessage.ERROR_BAD_REQUEST;

@Service
public class AccountService {

    private UserService userService;
    private final AccountRepository accountRepository;

    public AccountService(UserService userService,
                          AccountRepository accountRepository) {
        this.userService = userService;
        this.accountRepository = accountRepository;
    }

    public AccountDto getById(Long id) {
        return accountRepository.findById(id)
                                .map(AccountMapper.INSTANCE::toDto)
                                .orElseThrow(() ->
                                        new ClientException(ERROR_ACCOUNT_NOT_FOUND.getMessageBody(), HttpStatus.BAD_REQUEST)
                                );
    }

    public AccountDto getByAccountValueAndAccountType(String accountValue, String accountType) {
        return accountRepository.findByAccountValueAndAccountType(accountValue, accountType)
                                .map(AccountMapper.INSTANCE::toDto)
                                .orElseThrow(() ->
                                        new ClientException(ERROR_ACCOUNT_NOT_FOUND.getMessageBody(), HttpStatus.BAD_REQUEST)
                                );
    }

    public AccountDto insertOrUpdate(AccountDto accountDto) {
        Long userId = accountDto.getUser().getId();
        accountDto.setUser(userService.getById(userId));
        return AccountMapper.INSTANCE.toDto(
                accountRepository.save(
                        AccountMapper.INSTANCE.toEntity(
                                accountDto
                        )
                )
        );
    }

    public AccountDto updateAndSave(Long id,
                                    Map<String, Object> fieldsMap) {
        AccountDto accountDto = getById(id);
        fieldsMap.forEach((k, v) -> {
            Field field = ReflectionUtils.findField(AccountDto.class, k);
            if(field == null) {
                throw new ClientException(ERROR_BAD_REQUEST.getMessageBody(), HttpStatus.BAD_REQUEST);
            }
            field.setAccessible(true);
            ReflectionUtils.setField(field, accountDto, v instanceof Double ? BigDecimal.valueOf((Double)v) : v);
        });
        return insertOrUpdate(accountDto);
    }
}
