package az.aztu.dissertation.account.service;

import az.aztu.dissertation.account.dto.AuthUserDetailsDto;
import az.aztu.dissertation.account.dto.entity.UserDetailsDto;
import az.aztu.dissertation.account.dto.entity.UserDto;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

import static az.aztu.dissertation.account.enums.ErrorMessage.ERROR_USER_NOT_FOUND;

@Service
public class AuthUserDetailsService implements UserDetailsService {

    private final UserService userService;

    public AuthUserDetailsService(UserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        UserDto userDto = userService.getByUsername(username);

        if(userDto == null || userDto.getId() == null) {
            throw new UsernameNotFoundException(ERROR_USER_NOT_FOUND.getMessageBody());
        }

        return new AuthUserDetailsDto(
                userDto.getId(),
                userDto.getUsername(),
                userDto.getPassword(),
                userDto.getUserAuthorityList()
                       .stream()
                       .map(UserDetailsDto::getUserAuthority)
                       .collect(Collectors.toList())
        );
    }
}
