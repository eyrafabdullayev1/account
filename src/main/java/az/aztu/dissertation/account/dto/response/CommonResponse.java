package az.aztu.dissertation.account.dto.response;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.Serializable;
import java.time.LocalDateTime;

public class CommonResponse<T> implements Serializable {

    private Integer code;
    private String message;
    private T data;
    @DateTimeFormat(pattern = "dd-mm-yyyy HH:mm:ss")
    private LocalDateTime timestamp;

    public ResponseEntity<CommonResponse<?>> success(String message, T data) {
        return ResponseEntity.ok(
                new CommonResponse<T>().setCode(1)
                        .setMessage(message)
                        .setData(data)
                        .setTimestamp(LocalDateTime.now())
        );
    }

    public ResponseEntity<CommonResponse<?>> error(String message, HttpStatus statusCode) {
        return ResponseEntity.status(statusCode)
                .body(
                        new CommonResponse<>().setCode(-1)
                                .setMessage(message)
                                .setData(null)
                                .setTimestamp(LocalDateTime.now())
                );
    }

    public Integer getCode() {
        return code;
    }

    public CommonResponse<T> setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public CommonResponse<T> setMessage(String message) {
        this.message = message;
        return this;
    }

    public T getData() {
        return data;
    }

    public CommonResponse<T> setData(T data) {
        this.data = data;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public CommonResponse<T> setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }
}
