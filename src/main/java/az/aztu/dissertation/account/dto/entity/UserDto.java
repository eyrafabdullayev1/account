package az.aztu.dissertation.account.dto.entity;

import java.io.Serializable;
import java.util.List;

public class UserDto implements Serializable {

    private Long id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private List<UserDetailsDto> userAuthorityList;

    public Long getId() {
        return id;
    }

    public UserDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public UserDto setName(String name) {
        this.name = name;
        return this;
    }

    public String getSurname() {
        return surname;
    }

    public UserDto setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public UserDto setUsername(String username) {
        this.username = username;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public UserDto setPassword(String password) {
        this.password = password;
        return this;
    }

    public List<UserDetailsDto> getUserAuthorityList() {
        return userAuthorityList;
    }

    public UserDto setUserAuthorityList(List<UserDetailsDto> userAuthorityList) {
        this.userAuthorityList = userAuthorityList;
        return this;
    }
}
