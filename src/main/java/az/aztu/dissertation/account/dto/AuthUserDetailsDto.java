package az.aztu.dissertation.account.dto;

import az.aztu.dissertation.account.enums.UserAuthority;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AuthUserDetailsDto implements UserDetails {

    private final Long id;
    private final String pin;
    private final String password;
    private final Collection<? extends GrantedAuthority> authorityList;

    public AuthUserDetailsDto(Long id,
                              String pin,
                              String password,
                              List<UserAuthority> userAuthority) {
        this.id = id;
        this.pin = pin;
        this.password = password;

        List<SimpleGrantedAuthority> authorityList = new ArrayList<>();
        userAuthority.forEach(authority ->
                authorityList.add(
                        new SimpleGrantedAuthority(authority.name())
                )
        );

        this.authorityList = authorityList;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorityList;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.pin;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
