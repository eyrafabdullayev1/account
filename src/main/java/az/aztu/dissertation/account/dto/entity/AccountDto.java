package az.aztu.dissertation.account.dto.entity;

import az.aztu.dissertation.account.enums.AccountType;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountDto implements Serializable {

    private Long id;
    private String accountValue;
    private AccountType accountType;
    private BigDecimal balance;
    private Boolean isExpired;
    private UserDto user;

    public Long getId() {
        return id;
    }

    public AccountDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAccountValue() {
        return accountValue;
    }

    public AccountDto setAccountValue(String accountValue) {
        this.accountValue = accountValue;
        return this;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountDto setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public AccountDto setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }

    public UserDto getUser() {
        return user;
    }

    public AccountDto setUser(UserDto user) {
        this.user = user;
        return this;
    }
}
