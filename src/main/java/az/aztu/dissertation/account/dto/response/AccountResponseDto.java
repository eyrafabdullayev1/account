package az.aztu.dissertation.account.dto.response;

import az.aztu.dissertation.account.enums.AccountType;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountResponseDto implements Serializable {

    private Long id;
    private String accountValue;
    private AccountType accountType;
    private BigDecimal balance;
    private Boolean isExpired;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAccountValue() {
        return accountValue;
    }

    public void setAccountValue(String accountValue) {
        this.accountValue = accountValue;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public void setAccountType(AccountType accountType) {
        this.accountType = accountType;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public void setExpired(Boolean expired) {
        isExpired = expired;
    }
}
