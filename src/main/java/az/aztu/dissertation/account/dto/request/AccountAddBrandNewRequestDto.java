package az.aztu.dissertation.account.dto.request;

import az.aztu.dissertation.account.enums.AccountType;

import java.io.Serializable;
import java.math.BigDecimal;

public class AccountAddBrandNewRequestDto implements Serializable {

    private Long id;
    private String accountValue;
    private AccountType accountType;
    private BigDecimal balance;
    private Boolean isExpired;
    private Long userId;

    public Long getId() {
        return id;
    }

    public AccountAddBrandNewRequestDto setId(Long id) {
        this.id = id;
        return this;
    }

    public String getAccountValue() {
        return accountValue;
    }

    public AccountAddBrandNewRequestDto setAccountValue(String accountValue) {
        this.accountValue = accountValue;
        return this;
    }

    public AccountType getAccountType() {
        return accountType;
    }

    public AccountAddBrandNewRequestDto setAccountType(AccountType accountType) {
        this.accountType = accountType;
        return this;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public AccountAddBrandNewRequestDto setBalance(BigDecimal balance) {
        this.balance = balance;
        return this;
    }

    public Boolean getExpired() {
        return isExpired;
    }

    public AccountAddBrandNewRequestDto setExpired(Boolean expired) {
        isExpired = expired;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public AccountAddBrandNewRequestDto setUserId(Long userId) {
        this.userId = userId;
        return this;
    }
}
