package az.aztu.dissertation.account.dto.request;

import java.io.Serializable;

public class ParcelAddBrandNewRequestDto implements Serializable {

    private Long id;
    private Long accountId;
    private String description;
    private String destination;

    public Long getId() {
        return id;
    }

    public ParcelAddBrandNewRequestDto setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getAccountId() {
        return accountId;
    }

    public ParcelAddBrandNewRequestDto setAccountId(Long accountId) {
        this.accountId = accountId;
        return this;
    }

    public String getDescription() {
        return description;
    }

    public ParcelAddBrandNewRequestDto setDescription(String description) {
        this.description = description;
        return this;
    }

    public String getDestination() {
        return destination;
    }

    public ParcelAddBrandNewRequestDto setDestination(String destination) {
        this.destination = destination;
        return this;
    }
}
