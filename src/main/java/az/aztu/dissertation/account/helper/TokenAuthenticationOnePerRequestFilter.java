package az.aztu.dissertation.account.helper;

import az.aztu.dissertation.account.config.ApplicationProperties;
import az.aztu.dissertation.account.util.TokenUtil;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static az.aztu.dissertation.account.constraint.CommonConstraint.SPACE;
import static az.aztu.dissertation.account.enums.Message.*;

@Component
public class TokenAuthenticationOnePerRequestFilter extends OncePerRequestFilter {

    private final Logger logger;
    private final TokenUtil tokenUtil;
    private final UserDetailsService userDetailsService;
    private final ApplicationProperties applicationProperties;

    public TokenAuthenticationOnePerRequestFilter(Logger logger, TokenUtil tokenUtil, UserDetailsService userDetailsService, ApplicationProperties applicationProperties) {
        this.logger = logger;
        this.tokenUtil = tokenUtil;
        this.userDetailsService = userDetailsService;
        this.applicationProperties = applicationProperties;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        logger.debug("Authentication Request For '{}'", request.getRequestURL());

        final String tokenRequestHeader = request.getHeader(applicationProperties.getTokenJWTHeader());

        String username = null;
        String tokenExactly = null;
        if(StringUtils.isNotBlank(tokenRequestHeader)
           && tokenRequestHeader.startsWith(applicationProperties.getTokenJWTPrefix() + SPACE)) {
            tokenExactly = tokenRequestHeader.substring(applicationProperties.getTokenJWTPrefix().length()).trim();
            try {
                username = tokenUtil.getUsernameFromToken(tokenExactly);
            } catch (IllegalArgumentException ex) {
                logger.error(ERROR_TOKEN_UNABLE_USERNAME.getMessage());
            } catch (ExpiredJwtException ex) {
                logger.warn(WARN_TOKEN_EXPIRED.getMessage());
            }
        } else {
            logger.warn(String.format(WARN_TOKEN_DOES_NOT_START_WITH.getMessage(), applicationProperties.getTokenJWTPrefix()));
        }

        logger.debug("JWT_TOKEN_USERNAME_VALUE '{}'", username);
        if((username != null && !username.isBlank()) && SecurityContextHolder.getContext().getAuthentication() == null) {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);

            if(tokenUtil.validateToken(tokenExactly, userDetails)) {
                UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
                usernamePasswordAuthenticationToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
                SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);
            }
        }

        filterChain.doFilter(request, response);
    }
}
