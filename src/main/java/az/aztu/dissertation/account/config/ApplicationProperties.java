package az.aztu.dissertation.account.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ApplicationProperties {

    @Value("${token.jwt.header}")
    private String tokenJWTHeader;
    @Value("${token.jwt.signing-key}")
    private String tokenJWTSigningKey;
    @Value("${token.jwt.prefix}")
    private String tokenJWTPrefix;
    @Value("${token.jwt.expiration-in-seconds}")
    private Long tokenJWTExpirationInSeconds;

    public String getTokenJWTHeader() {
        return tokenJWTHeader;
    }

    public String getTokenJWTSigningKey() {
        return tokenJWTSigningKey;
    }

    public String getTokenJWTPrefix() {
        return tokenJWTPrefix;
    }

    public Long getTokenJWTExpirationInSeconds() {
        return tokenJWTExpirationInSeconds;
    }
}
