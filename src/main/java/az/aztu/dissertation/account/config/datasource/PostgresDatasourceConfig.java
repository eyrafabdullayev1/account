package az.aztu.dissertation.account.config.datasource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(
        basePackages = "az.aztu.dissertation.account.repository",
        entityManagerFactoryRef = "postgres-emf",
        transactionManagerRef = "postgres-tm"
)
@EnableTransactionManagement
public class PostgresDatasourceConfig {

    @Value("${datasource.postgres.url}")
    private String url;
    @Value("${datasource.postgres.user}")
    private String user;
    @Value("${datasource.postgres.password}")
    private String password;
    @Value("${datasource.postgres.driver-class-name}")
    private String driverClassName;
    @Value("${datasource.postgres.hibernate-dialect}")
    private String hibernateDialect;

    @Bean
    public DataSource dataSource() {
        System.out.println("url:"+url);
        return DataSourceBuilder.create()
                .url(url)
                .username(user)
                .password(password)
                .driverClassName(driverClassName)
                .build();
    }

    @Bean
    public HashMap<String, Object> jpaPropertyMap() {
        HashMap<String, Object> propertyMap = new HashMap<>();
        propertyMap.put("hibernate.dialect", hibernateDialect);
        propertyMap.put("hibernate.show_sql", true);
        propertyMap.put("hibernate.format_sql", true);
        return propertyMap;
    }

    @Bean("postgres-emf")
    public LocalContainerEntityManagerFactoryBean entityManagerFactory() {
        LocalContainerEntityManagerFactoryBean entityManagerFactory = new LocalContainerEntityManagerFactoryBean();
        entityManagerFactory.setDataSource(dataSource());
        entityManagerFactory.setJpaPropertyMap(jpaPropertyMap());
        entityManagerFactory.setPackagesToScan("az.aztu.dissertation.account.entity");
        HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
        entityManagerFactory.setJpaVendorAdapter(hibernateJpaVendorAdapter);
        return entityManagerFactory;
    }

    @Bean("postgres-tm")
    public PlatformTransactionManager transactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory().getObject());
        return transactionManager;
    }
}
