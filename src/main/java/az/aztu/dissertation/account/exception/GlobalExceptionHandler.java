package az.aztu.dissertation.account.exception;

import az.aztu.dissertation.account.dto.response.CommonResponse;
import az.aztu.dissertation.account.exception.custom.ClientException;
import az.aztu.dissertation.account.exception.custom.ExternalServiceException;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler({
            ClientException.class
    })
    public ResponseEntity<CommonResponse<?>> handleClientException(
            ClientException ex
    ) {
        return new CommonResponse<>().error(ex.getMessage(), ex.getStatus());
    }

    @ExceptionHandler({
            ExternalServiceException.class
    })
    public ResponseEntity<CommonResponse<?>> handleInternalServiceException(
            ExternalServiceException ex
    ) {
        return new CommonResponse<>().error(ex.getMessage(), ex.getStatus());
    }

    @ExceptionHandler({
            Exception.class
    })
    public ResponseEntity<CommonResponse<?>> handleUnknownException(
            Exception ex
    ) {
        return new CommonResponse<>().error(ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
