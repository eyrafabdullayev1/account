package az.aztu.dissertation.account.exception.custom;

public class BaseException extends RuntimeException {

    public BaseException() {

    }

    public BaseException(String message) {
        super(message);
    }
}
