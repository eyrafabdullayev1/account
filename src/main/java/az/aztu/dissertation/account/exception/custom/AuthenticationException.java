package az.aztu.dissertation.account.exception.custom;

import org.springframework.http.HttpStatus;

public class AuthenticationException extends ClientException {

    public AuthenticationException(String message, HttpStatus status) {
        super(message, status);
    }

    public AuthenticationException(String message, HttpStatus status, Throwable cause) {
        super(message, status, cause);
    }
}
